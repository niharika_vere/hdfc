package com.bank.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bank.dto.CustomerResgistrationDto;
import com.bank.dto.ResponseDto;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.UserAlreadyExists;
import com.bank.repository.CustomerRegistrationRepository;
import com.bank.service.CustomerRegistrationServiceImpl;
 
@ExtendWith(SpringExtension.class)
class CustomerRegistrationServiceImplTest {
	
	@Mock
	CustomerRegistrationRepository customerRegistrationRepository;
	
	@InjectMocks
	CustomerRegistrationServiceImpl customerRegistrationServiceImpl;
	
 
    @Test
     void testRegister_NewUser_Success() {
        CustomerResgistrationDto registrationDto = new CustomerResgistrationDto();
 
        
        when(customerRegistrationRepository.findByAdhaarNo(anyString())).thenReturn(Optional.empty());
 
        CustomerRegistrationServiceImpl registrationService = new CustomerRegistrationServiceImpl(customerRegistrationRepository);
         ResponseDto response = registrationService.register(registrationDto);
        verify(customerRegistrationRepository, times(1)).save(any(CustomerRegistration.class));
    }
 
    @Test
    void testRegister_ExistingUser_ThrowsException() {
    	
    	CustomerRegistration customerRegistration=new CustomerRegistration();
    	customerRegistration.setAdhaarNo("768908764678");
       
       CustomerResgistrationDto customerResgistrationDto=new CustomerResgistrationDto();
       customerResgistrationDto.setAdhaarNo("678956780956");
        when(customerRegistrationRepository.findByAdhaarNo(customerResgistrationDto.getAdhaarNo())).thenReturn(Optional.of(customerRegistration));
        UserAlreadyExists exception= assertThrows(UserAlreadyExists.class, () -> customerRegistrationServiceImpl.register(customerResgistrationDto));
        
        assertEquals("User already Registered",exception.getMessage());
    }
    
    @Test
     void testRegister_NewUserWithAllInfo_Success() {
        CustomerResgistrationDto registrationDto = new CustomerResgistrationDto();
        registrationDto.setFirstName("Niharika");
        registrationDto.setLastName("Royal");
        registrationDto.setAdhaarNo("123456789012");
        registrationDto.setAddress("Kurnoo");
        registrationDto.setContactNo("9876543210");
 
        when(customerRegistrationRepository.findByAdhaarNo(anyString())).thenReturn(Optional.empty());
        when(customerRegistrationRepository.save(any(CustomerRegistration.class))).thenReturn(new CustomerRegistration());
 
        CustomerRegistrationServiceImpl registrationService = new CustomerRegistrationServiceImpl(customerRegistrationRepository);
         ResponseDto response = registrationService.register(registrationDto);
         verify(customerRegistrationRepository, times(1)).save(any(CustomerRegistration.class));
        assertEquals(HttpStatus.CREATED.value(), response.getHttpStatus());
        assertEquals("User Registered sucessfully", response.getMessage());
    }
    @Test
     void testRegister_NewUserWithMinimumInfo_Success() {
        CustomerResgistrationDto registrationDto = new CustomerResgistrationDto();
        registrationDto.setFirstName("Niharika");
        registrationDto.setAdhaarNo("123456789012");
 
        when(customerRegistrationRepository.findByAdhaarNo(anyString())).thenReturn(Optional.empty());
        when(customerRegistrationRepository.save(any(CustomerRegistration.class))).thenReturn(new CustomerRegistration());
 
        CustomerRegistrationServiceImpl registrationService = new CustomerRegistrationServiceImpl(customerRegistrationRepository);
         ResponseDto response = registrationService.register(registrationDto);
         verify(customerRegistrationRepository, times(1)).save(any(CustomerRegistration.class));
        assertEquals(HttpStatus.CREATED.value(), response.getHttpStatus());
        assertEquals("User Registered sucessfully", response.getMessage());
    }
    
 
}
 

