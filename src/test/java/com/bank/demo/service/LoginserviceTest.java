//package com.bank.demo.service;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.Mockito.when;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import com.bank.dto.LoginDto;
//import com.bank.entity.CustomerRegistration;
//import com.bank.exception.InvalidCredentialsException;
//import com.bank.repository.LoginRepository;
//import com.bank.service.Loginservice;
//
//@ExtendWith(MockitoExtension.class)
//public class LoginserviceTest {
//   @Mock
//   private LoginRepository loginRepository;
//   @InjectMocks
//   private Loginservice loginService;
//   @Test
//   void testLoginWithValidCredentials() {
//       LoginDto validDto = new LoginDto("validCustomerId", "validPassword");
//       CustomerRegistration validCustomer = new CustomerRegistration();
//       validCustomer.setCustomerId("validCustomerId");
//       validCustomer.setPassword("validPassword");
//       when(loginRepository.findByCustomerId("validCustomerId")).thenReturn(validCustomer);
//       CustomerRegistration result = loginService.login(validDto);
//       assertNotNull(result);
//       assertEquals("validCustomerId", result.getCustomerId());
//   }
//   @Test
//   void testLoginWithInvalidCredentials() {
//       LoginDto invalidDto = new LoginDto("invalidCustomerId", "invalidPassword");
//       when(loginRepository.findByCustomerId("invalidCustomerId")).thenReturn(null);
//       assertThrows(InvalidCredentialsException.class, () -> loginService.login(invalidDto));
//   }
//}