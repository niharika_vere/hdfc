//package com.bank.demo.service;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.when;
// 
//import java.util.Arrays;
//import java.util.List;
// 
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
// 
//import com.bank.dto.AccountDTO;
//import com.bank.entity.CustomerRegistration;
//import com.bank.repository.AccountRepository;
//import com.bank.service.AccountService;
//@ExtendWith(MockitoExtension.class)
//public class AccountServiceTest {
//   @Mock
//   private AccountRepository accountRepository;
//   @InjectMocks
//   private AccountService accountService;
//   @Test
//   public void testGetDataByColumnName() {
//       CustomerRegistration registration1 = new CustomerRegistration();
//       registration1.setCustomerId("123");
//       registration1.setAccountNo("AC123");
//       registration1.setIfscCode("IFSC123");
//       registration1.setBankName("Bank A");
//       registration1.setBranch("Branch A");
//       registration1.setBalance(100.0);
//       CustomerRegistration registration2 = new CustomerRegistration();
//       registration2.setCustomerId("123");
//       registration2.setAccountNo("AC456");
//       registration2.setIfscCode("IFSC456");
//       registration2.setBankName("Bank B");
//       registration2.setBranch("Branch B");
//       registration2.setBalance(200.0);
//       List<CustomerRegistration> mockEntities = Arrays.asList(registration1, registration2);
//       when(accountRepository.findByCustomerId(anyString())).thenReturn(mockEntities);
//       List<AccountDTO> result = accountService.getDataByColumnName("123");
//       assertEquals(2, result.size());
//       assertEquals("123", result.get(0).getCustomerId());
//       assertEquals("AC123", result.get(0).getAccountNo());
//       assertEquals("IFSC123", result.get(0).getIfscCode());
//       assertEquals("Bank A", result.get(0).getBankName());
//       assertEquals("Branch A", result.get(0).getBranch());
//       assertEquals(100.0, result.get(0).getBalance());
//       assertEquals("123", result.get(1).getCustomerId());
//       assertEquals("AC456", result.get(1).getAccountNo());
//       assertEquals("IFSC456", result.get(1).getIfscCode());
//       assertEquals("Bank B", result.get(1).getBankName());
//       assertEquals("Branch B", result.get(1).getBranch());
//       assertEquals(200.0, result.get(1).getBalance());
//   }
//}