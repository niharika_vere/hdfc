//package com.bank.demo.controller;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//
//import com.bank.controller.Logincontroller;
//import com.bank.dto.LoginDto;
//import com.bank.dto.loginresponse;
//import com.bank.entity.CustomerRegistration;
//import com.bank.service.Loginservice;
//@ExtendWith(MockitoExtension.class)
// class LoginControllerTest {
//   @Mock
//   private Loginservice mockLoginService;
//   @InjectMocks
//   private Logincontroller loginController;
//   @Test
//    void testLogin_Success() {
//       LoginDto validDto = new LoginDto("validCustomerId", "validPassword");
//       when(mockLoginService.login(any(LoginDto.class))).thenReturn(new CustomerRegistration());
//       ResponseEntity<loginresponse> responseEntity = loginController.login(validDto);
//       assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//       assertEquals("Login successful", responseEntity.getBody().getMessage());
//   }
//   @Test
//    void testLogin_NullDto() {
//       ResponseEntity<loginresponse> responseEntity = loginController.login(null);
//       assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
//       assertEquals("Login data is required", responseEntity.getBody().getMessage());
//   }
//}
