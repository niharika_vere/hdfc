package com.bank.demo.controller;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
 
import java.util.Arrays;
import java.util.List;
 
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
 
import com.bank.controller.AccountController;
import com.bank.dto.AccountDTO;
import com.bank.service.AccountService;
@ExtendWith(MockitoExtension.class)
class AccountControllerTest {
   @Mock
   private AccountService accountService;
   @InjectMocks
   private AccountController accountController;
   @Test
    void testGetData() {
       String customerId = "123";
       List<AccountDTO> expectedData = Arrays.asList(new AccountDTO(/*account details*/));
       when(accountService.getDataByColumnName(customerId)).thenReturn(expectedData);
       List<AccountDTO> actualData = accountController.getData(customerId);
       assertEquals(expectedData, actualData);
   }
}