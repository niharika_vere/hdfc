package com.bank.demo.controller;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bank.controller.CustomerRegistrationController;
import com.bank.dto.CustomerResgistrationDto;
import com.bank.dto.ResponseDto;
import com.bank.service.CustomerRegistrationServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class CustomerRegistrationControllerTest {
 
    @Mock
    private CustomerRegistrationServiceImpl registrationService;
 
    @InjectMocks
    private CustomerRegistrationController controller;
 
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
 
    @Test
    void testRegister_Success() {
        CustomerResgistrationDto registrationDto = new CustomerResgistrationDto( "Niharika","Royal","9966969296","602421253496","Kurnool");
        ResponseDto expectedResponse = new ResponseDto("User Registered sucessfully", (long) 2011);
        when(registrationService.register(registrationDto)).thenReturn(expectedResponse);
 
    
        ResponseEntity<ResponseDto> responseEntity = controller.register(registrationDto);
 
        
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
        verify(registrationService, times(1)).register(registrationDto);
    }
    @Test
    void testRegister_DuplicateRegistration() {
  
        CustomerResgistrationDto registrationDto = new CustomerResgistrationDto("Niharika","Royal","9966969296","602421253496","Kurnool");
        when(registrationService.register(registrationDto)).thenReturn(new ResponseDto("User already Registered", (long) 2011));
     
        ResponseEntity<ResponseDto> responseEntity = controller.register(registrationDto);
     
        assertNotEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
        
    }

 
}