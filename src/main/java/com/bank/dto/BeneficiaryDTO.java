package com.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class BeneficiaryDTO {
	 
    private String beneficiaryName;
    private String beneficiaryBankName;
    private String beneficiaryBranch;
    private String beneficiaryIFSCCode;
}