package com.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
 
public class AccountDTO {
 
	private String customerId; 
	private String accountNo;
	private String ifscCode; 
	private String bankName; 
	private String branch; 
	private String balance;
}
