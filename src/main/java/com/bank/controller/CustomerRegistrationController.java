package com.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.CustomerResgistrationDto;
import com.bank.dto.ResponseDto;
import com.bank.service.CustomerRegistrationServiceImpl;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class CustomerRegistrationController {
	@Autowired
	CustomerRegistrationServiceImpl customerRegistrationServiceImpl;

	public CustomerRegistrationController(CustomerRegistrationServiceImpl customerRegistrationServiceImpl) {
		super();
		this.customerRegistrationServiceImpl = customerRegistrationServiceImpl;

	}
	@PostMapping("/register")
	public ResponseEntity<ResponseDto> register(@Valid @RequestBody CustomerResgistrationDto customerResgistrationDto){
		return new ResponseEntity<ResponseDto>(customerRegistrationServiceImpl.register(customerResgistrationDto),HttpStatus.CREATED);
	}
}
