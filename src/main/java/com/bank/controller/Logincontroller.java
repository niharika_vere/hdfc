package com.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.LoginDto;
import com.bank.dto.ResponseDto;
import com.bank.service.Loginservice;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/")
@Validated
public class Logincontroller {
	@Autowired
	private Loginservice loginservice;

//	@PostMapping("/login")
//	public ResponseEntity<loginresponse> login(@RequestBody LoginDto dto) {
//		try {
//			validateLoginDto(dto);
//			CustomerRegistration customer = loginservice.login(dto);
//			return ResponseEntity.ok(new loginresponse("Login successful"));
//		} catch (ValidationException ex) {
//			return ResponseEntity.badRequest().body(new loginresponse(ex.getMessage()));
//		} catch (InvalidCredentialsException ex) {
//			return ResponseEntity.badRequest().body(new loginresponse(ex.getMessage()));
//		}
//	}
	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(loginservice.login(loginDto), HttpStatus.OK);
		
		
//	private void validateLoginDto(LoginDto dto) {
//		if (dto == null) {
//			throw new ValidationException("Login data is required");
//		}
//		if (dto.getCustomerId() == null || dto.getCustomerId().isEmpty()) {
//			throw new ValidationException("Customer ID is required");
//		}
//		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
//			throw new ValidationException("Password is required");
//		}
//	}
	}
}
