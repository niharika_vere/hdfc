package com.bank.entity;

import com.bank.dto.LogInStatus;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class CustomerRegistration {
	@Id
	private String customerId;
	private String firstName;
	private String lastName;
	private String contactNo;
	private String	adhaarNo;
	private String password;
	private String address;
	private String accountNo;
	private String balance;
	private String bankName;
	private String ifscCode;
	private String branch;
	@Enumerated(EnumType.STRING)
	private LogInStatus loggedIn;
//	public static Object builder() {
//		return null;
//	}

}
