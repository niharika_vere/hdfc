package com.bank.exception;

public class InvalidCredentialsException extends RuntimeException {
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -4834062676656675050L;

	public InvalidCredentialsException(String message) {
		super(message);
	}
}
