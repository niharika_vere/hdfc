package com.bank.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dto.AccountDTO;
import com.bank.entity.CustomerRegistration;
import com.bank.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	public List<AccountDTO> getDataByColumnName(String customerId) {
		List<CustomerRegistration> entities = accountRepository.findByCustomerId(customerId);
		return entities.stream().map(this::mapEntityToDTO).collect(Collectors.toList());
	}

	private AccountDTO mapEntityToDTO(CustomerRegistration entity) {
		AccountDTO dto = new AccountDTO();
		dto.setCustomerId(entity.getCustomerId());
		dto.setAccountNo(entity.getAccountNo());
		dto.setIfscCode(entity.getIfscCode());
		dto.setBankName(entity.getBankName());
		dto.setBranch(entity.getBranch());
		dto.setBalance(entity.getBalance());
		return dto;
	}
}
