package com.bank.service;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dto.LogInStatus;
import com.bank.dto.LoginDto;
import com.bank.dto.ResponseDto;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ProfileNotFoundException;
import com.bank.repository.CustomerRegistrationRepository;
import com.bank.repository.LoginRepository;
@Service
public class Loginservice {
	@Autowired
	private LoginRepository loginrepository;
	@Autowired
	CustomerRegistrationRepository customerRegistrationRepository;
	
	
//	public CustomerRegistration login(@Valid LoginDto dto) {
//			String customerId = dto.getCustomerId();
//	        String password = dto.getPassword();
//	 
//	        CustomerRegistration customer = loginrepository.findByCustomerId(customerId);
//	 
//	        if (customer != null && customer.getPassword().equals(password)) {
//	            return customer;
//	        } else {
//	            throw new InvalidCredentialsException("Invalid Login Credentials");
//	        }
//			
//		}
	
	public ResponseDto login(LoginDto loginDto) {
		org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
		System.out.println(loginDto.toString());
		CustomerRegistration customerRegistration = customerRegistrationRepository.findByCustomerIdAndPassword(loginDto.getCustomerId(), loginDto.getPassword());
		System.err.println(customerRegistration);
		if (customerRegistration != null && loginDto.getPassword().equals(customerRegistration.getPassword())) {
			customerRegistration.setLoggedIn(LogInStatus.LOGIN);
			customerRegistrationRepository.save(customerRegistration);
			logger.info("Profile Logged in successfully");
			return new ResponseDto("Logged In successfully",201l);
		} else {
			logger.error("Invalid Credentials or profile Not found");
			throw new ProfileNotFoundException("Invalid Credentials or profile Not Found.");
		}
	}
}
