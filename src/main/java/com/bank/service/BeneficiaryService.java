package com.bank.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.bank.dto.BeneficiaryDTO;
import com.bank.dto.BeneficiaryResponseDTO;
import com.bank.entity.Beneficiary;
import com.bank.entity.CustomerRegistration;
import com.bank.repository.BeneficiaryRepository;
import com.bank.repository.CustomerRegistrationRepository;


@Service
public class BeneficiaryService {
	 
    @Autowired
    private BeneficiaryRepository beneficiaryRepository;
 
    @Autowired
    private CustomerRegistrationRepository registrationRepository;
 
    public BeneficiaryResponseDTO addBeneficiary(String customerId, BeneficiaryDTO beneficiaryDTO) {
        Optional<CustomerRegistration> registrationOptional = registrationRepository.findByCustomerId(customerId);
        if (registrationOptional.isPresent()) {
            CustomerRegistration registration = registrationOptional.get();
            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setBeneficiaryName(beneficiaryDTO.getBeneficiaryName());
            beneficiary.setBeneficiaryBankName(beneficiaryDTO.getBeneficiaryBankName());
            beneficiary.setBeneficiaryBranch(beneficiaryDTO.getBeneficiaryBranch());
            beneficiary.setBeneficiaryIFSCCode(beneficiaryDTO.getBeneficiaryIFSCCode());
            beneficiary.setRegistration(registration);
            beneficiaryRepository.save(beneficiary);
            
            String message = "Beneficiary Added Successfully...";
            BeneficiaryResponseDTO responsedto = new BeneficiaryResponseDTO(message, registration.getCustomerId());
            return responsedto;
        } else {
            throw new ResourceNotFoundException("Customer with ID " + customerId + " not found");
        }
    }
 }