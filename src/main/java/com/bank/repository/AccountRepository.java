package com.bank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bank.entity.CustomerRegistration;

public interface AccountRepository extends JpaRepository<CustomerRegistration, String> {

	List<CustomerRegistration> findByCustomerId(String customerId);
}
