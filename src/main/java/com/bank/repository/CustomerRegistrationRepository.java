package com.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.entity.CustomerRegistration;

@Repository
public interface CustomerRegistrationRepository extends JpaRepository<CustomerRegistration, String> {

	Optional<CustomerRegistration> findByAdhaarNo(String aadharNo);
	Optional<CustomerRegistration> findByCustomerId(String customerId);
	CustomerRegistration findByCustomerIdAndPassword(String customerId, String password);
	

}
